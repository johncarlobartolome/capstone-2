//set up the dependencies
//require is used to relate other files to our current file
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

//import routes
const productRoutes = require("./routes/productRoutes");
const userRoutes = require("./routes/userRoutes");
const orderRoutes = require("./routes/orderRoutes");

//Server setup
const app = express();
const port = 6000;

app.use(cors());
//tells server that cors is being used by your server

app.use(express.json());
app.use(express.urlencoded({extended: true}));

// to access yung courseRoutes dapat ganito localhost:3000/courses
app.use("/products", productRoutes)
app.use("/users", userRoutes)
app.use("/orders", orderRoutes)

mongoose.connect("mongodb+srv://admin:admin123@cluster0.alqqh.mongodb.net/b110_ecommerce?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
})
//

//mongoose connection confirmation
mongoose.connection.once('open', () => {
	console.log("Now connected to MongoDB Atlas.")
})


//makes the actual connection
app.listen(process.env.PORT || port, () => {
	console.log(`API is now online on port ${process.env.PORT || port
	}`)
})

//process.env.port = env = environment variables - to set variables that depend on the environment //like an else statment, if cannot use the 3000 port, it will use the host port na using
//|| on above 
//the PORT there sir corresponds to assigned port ng hosting server? - yes, correct