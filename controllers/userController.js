const User = require("../models/user");
const Product = require("../models/product");
const auth = require("../auth");
const bcrypt = require("bcrypt");



//CHECK EXISTING EMAIL
module.exports.checkEmail = (body) => {
	return User.find({email: body.email}).then(result => {
		if (result.length > 0) {
			return true;
		} else {
			return false
		}
	})
}


//CREATE A NEW USER
module.exports.registerUser = (body) => {
	let newUser = new User ({
		firstName: body.firstName,
		lastName: body.lastName,
		email: body.email,
		username: body.username,
		password: bcrypt.hashSync(body.password, 10),
		mobileNo: body.mobileNo,
		address: body.address,
		birthday: body.birthday,
		gender: body.gender
	})

	return newUser.save().then((user, error) => {
		if (error) {
			return false;
		}else{
			return true;
		}
	})
}


//LOG IN
module.exports.loginUser = (body) => {
	return User.findOne({email:body.email}).then(result => {
		if (result === null) {
			return false;
		}else{
			const isPasswordCorrect = bcrypt.compareSync(body.password, result.password)

			if (isPasswordCorrect) {
				return {access: auth.createAccessToken(result.toObject())}
			}else{
				return false;
			}
		}
	})
}


//GET USER DETAILS

module.exports.getProfile = (userId) => {
	return User.findById(userId).then(result => {
		result.password = undefined;
		return result;
	})
}



//**************

//CHECK OUT ORDER
module.exports.checkout = async(data) => {
	let productSave = await Product.findById(data.productId).then(product => {
		product.orders.push({userId: data.userId, quantity: data.quantity, totalAmount: data.totalAmount})

		return product.save().then((product, err) => {
			if (err) {
				return false;
			} else {
				return true;
			}
		})
	})

	let userSave = await User.findById(data.userId).then(user => {
		user.purchases.push({productId: data.productId, quantity: data.quantity, totalAmount: data.totalAmount})

		return user.save().then((user, err) => {
			if (err) {
				return false;
			} else {
				return true;
			}
		})
	})

	if (productSave && userSave) {
		return true;
	} else {
		return false;
	}
}


//GET USER PURCHASES
module.exports.getPurchases = (userId) => {
	return User.findById(userId).then(result => {
		// console.log(result)
		const {purchases} = result
		return purchases
	})
}




// //CHANGE TO ADMIN***********************************

module.exports.changeToAdmin = (params, body) => {
	let updatedUser = {
		isAdmin: body.isAdmin
	}

	return User.findByIdAndUpdate(params.userId, updatedUser).then((user, error) => {
		if (error) {
			return false;
		}else{
			return true;
		}
	})
}