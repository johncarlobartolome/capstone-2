const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
	userId: {
		type: String,
		required: [true, "Username is required"]
	},
	orderDetails: [
		{
			productId: {
				type: String,
				required: [true, "Name is required"]
			},
			quantity: {
				type: Number,
				required: [true, "Quantity is required"]
			}
		}
	],
	purchasedOn: {
		type: Date
		default: new Date ()
	},
	isActive: {
		type: Boolean,
		default: true
	}
	totalAmount: {
		type: Number,
		required: [true, "Total Amount is required"]
	}

})

module.exports = mongoose.model("Order", orderSchema);