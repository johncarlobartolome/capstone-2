const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController");
const auth = require("../auth");


//CREATE PRODUCT
router.post("/", auth.verify, (req, res) => {
	let token = auth.decode(req.headers.authorization)
	if (token.isAdmin === true) {
	productController.addProduct(req.body).then(resultFromAddProduct => res.send(resultFromAddProduct))
	}else{
		res.send({auth: "Not an admin"})
	}
})


//GET ALL ACTIVE PRODUCTS
router.get("/all", (req, res) => {
	productController.getAllActive(req.body).then(resultFromGetActive => res.send(resultFromGetActive))
})



//GET SPECIFIC PRODUCT
router.get("/:productId", (req, res) => {
	productController.getProduct(req.params).then(resultFromGetSpecific => res.send(resultFromGetSpecific))
})


//UPDATE A PRODUCT
router.put("/:productId", auth.verify, (req, res) => {
	productController.updateProduct(req.params, req.body).then(resultFromUpdate => res.send(resultFromUpdate))
})


//ARCHIVE PRODUCT
router.delete("/:productId", auth.verify, (req, res) => {
	productController.archive(req.params).then(resultFromArchive => res.send(resultFromArchive))
})


//GET PRODUCTS BY ADMIN
router.get("/admin", auth.verify, (req, res) => {
	// let token = auth.decode(req.headers.authorization)
	// if (token.isAdmin === true) {
	productController.adminView(req.body).then(result => res.send(result))
	// }else{
	// 	res.send({auth: "Not an admin"})
	// }
})

module.exports = router;